///=================================================================================================
// Class EphemerisGPS
//      Author :  Jose Gilberto RESENDIZ FONSECA
///=================================================================================================
/*
 * Copyright 2018(c) IFSTTAR - TeamGEOLOC
 *
 * This file is part of the GeolocPVT application.
 *
 * GeolocPVT is distributed as a free software in order to build a community of users, contributors,
 * developers who will contribute to the project and ensure the necessary means for its evolution.
 *
 * GeolocPVT is a free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version. Any modification of source code in this
 * LGPL software must also be published under the LGPL license.
 *
 * GeolocPVT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU Lesser General Public License along with GeolocPVT.
 * If not, see <https://www.gnu.org/licenses/lgpl.txt/>.
 */
///=================================================================================================

package fr.ifsttar.geoloc.geoloclib.satellites;

import android.location.GnssNavigationMessage;
import android.location.GnssStatus;

import org.gogpsproject.ephemeris.GNSSEphemeris;

/**
 * Class EphemerisGPS
 */
public class EphemerisGPS {
    //sub-frames needed to recuperate all ephemeris
    private NavigationMessage subFrame1;
    private NavigationMessage subFrame2;
    private NavigationMessage subFrame3;

    private int id; //satellite id
    private long tow; //time of week

    //subframe 1 (time corrections)
    private int wn;         //week number
    private double toc;     //clock data reference time in seconds
    private double af2;     //polynomial coefficient (sec/sec^2)
    private double af1;     //polynomial coefficient (sec/sec)
    private double af0;     //polynomial coefficient (seconds)

    //subframe 2 (ephemerides part 1)
    private double crs;     //Amplitude of the Sine Harmonic Correction Term to the Orbit Radius
    private double deltaN;  //Mean Motion Difference From Computed Value
    private double m0;      //Mean Anomaly at Reference Time
    private double cuc;     //Amplitude of the Cosine Harmonic Correction Term to the Argument of Latitude
    private double ec;      //Eccentricity
    private double cus;     //Amplitude of the Sine Harmonic Correction Term to the Argument of Latitude
    private double squareA; //Square Root of the Semi-Major Axis
    private double toe;     //Reference Time Ephemeris
    private double aodo;    //Age of data offset

    //subframe 3 (ephemerides part 2)
    private double cic;     //Amplitude of the Cosine Harmonic Correction Term to the Angle of Inclination
    private double omega0;  //Longitude of Ascending Node of Orbit Plane at Weekly Epoch
    private double cis;     //Amplitude of the Sine Harmonic Correction Term to the Angle of Inclination
    private double i0;      //Inclination Angle at Reference Time
    private double crc;     //Amplitude of the Cosine Harmonic Correction Term to the Orbit Radius
    private double omega;   //Argument of Perigee
    private double omegaDot;//Rate of Right Ascension
    private double idot;    //Rate of Inclination Angle

    /**
     * Constructor with parameters
     * @param subFrame1 sub-frame1
     * @param subFrame2 sub-frame2
     * @param subFrame3 sub-frame3
     */
    public EphemerisGPS(GnssNavigationMessage subFrame1 ,GnssNavigationMessage subFrame2, GnssNavigationMessage subFrame3) {
        this.subFrame1 = new NavigationMessage(subFrame1);
        this.subFrame2 = new NavigationMessage(subFrame2);
        this.subFrame3 = new NavigationMessage(subFrame3);

        id = setId();
        tow = setTow();

        wn = setWn();
        toc = setToc();
        af2 = setAf2();
        af1 = setAf1();
        af0 = setAf0();

        crs = setCrs();
        deltaN = setDeltaN();
        m0 = setM0();
        cuc = setCuc();
        ec = setEc();
        cus = setCus();
        squareA = setSquareA();
        toe = setToe();
        aodo = setAodo();

        cic = setCic();
        omega0 = setOmega0();
        cis = setCis();
        i0 = setI0();
        crc = setCrc();
        omega = setOmega();
        omegaDot = setOmegaDot();
        idot = setIdot();
    }

    //setting satellite id
    private int setId(){
        return subFrame1.getId();
    }

    //getting satellite id
    public int getId(){
        return id;
    }

    //all methods from here to method toString are methods to decode the corresponding value
    private long setTow(){
        return subFrame2.getTow();
    }

    public long getTow(){
        return tow;
    }

    private int setWn(){
        return (int)toDecimalfromBinaryString(this.subFrame1.getWord(2).substring(0,10), false);
    }

    public int getWn(){
        return wn;
    }

    private double setToc(){
        return toDecimalfromBinaryString(this.subFrame1.getWord(7).substring(8,24), false)
                * Math.pow(2,4);
    }

    public double getToc(){
        return toc;
    }

    private double setAf2(){
        return toDecimalfromBinaryString(this.subFrame1.getWord(8).substring(0,8),true)
                * Math.pow(2,-55);
    }

    public double getAf2(){
        return af2;
    }

    private double setAf1(){
        return toDecimalfromBinaryString(this.subFrame1.getWord(8).substring(8,24),true)
                * Math.pow(2,-43);
    }

    public double getAf1(){
        return af1;
    }

    private double setAf0(){
        return toDecimalfromBinaryString(this.subFrame1.getWord(9).substring(0,22),true)
                * Math.pow(2,-31);
    }

    public double getAf0(){
        return af0;
    }

    private double setCrs(){
        return toDecimalfromBinaryString(this.subFrame2.getWord(2).substring(8,24), true)
                * Math.pow(2,-5);
    }

    public double getCrs(){
        return crs;
    }

    private double setDeltaN(){
        return toDecimalfromBinaryString(this.subFrame2.getWord(3).substring(0,16), true)
                * Math.pow(2,-43) * Math.PI;
    }

    public double getDeltaN(){
        return deltaN;
    }

    private double setM0(){
        return toDecimalfromBinaryString(this.subFrame2.getWord(3).substring(16,24)
                + this.subFrame2.getWord(4).substring(0,24), true) * Math.pow(2,-31) * Math.PI;
    }
    public double getM0(){
        return m0;
    }

    private double setCuc(){
        return toDecimalfromBinaryString(this.subFrame2.getWord(5).substring(0,16), true)
                * Math.pow(2,-29);
    }
    public double getCuc(){
        return cuc;
    }

    private double setEc(){
        return toDecimalfromBinaryString(this.subFrame2.getWord(5).substring(16,24)
                + this.subFrame2.getWord(6).substring(0,24), false) * Math.pow(2,-33);
    }

    public double getEc(){
        return ec;
    }

    private double setCus(){
        return toDecimalfromBinaryString(this.subFrame2.getWord(7).substring(0,16), true)
                * Math.pow(2,-29);
    }
    public double getCus(){
        return cus;
    }

    private double setSquareA(){
        return toDecimalfromBinaryString(this.subFrame2.getWord(7).substring(16,24)
                + this.subFrame2.getWord(8).substring(0,24), false) * Math.pow(2,-19);
    }
    public double getSquareA(){
        return squareA;
    }

    private double setToe(){
        return toDecimalfromBinaryString(this.subFrame2.getWord(9).substring(0,16), false)
                * Math.pow(2,4);
    }
    public double getToe(){
        return toe;
    }

    private double setAodo(){
        return toDecimalfromBinaryString(this.subFrame2.getWord(9).substring(17,22),false)
                * 900;
    }
    public double getAodo(){
        return aodo;
    }

    private double setCic(){
        return toDecimalfromBinaryString(this.subFrame3.getWord(2).substring(0,16), true)
                * Math.pow(2,-29);
    }
    public double getCic(){
        return cic;
    }

    private double setOmega0(){
        return toDecimalfromBinaryString(this.subFrame3.getWord(2).substring(16,24)
                + this.subFrame3.getWord(3).substring(0,24), true) * Math.pow(2,-31) * Math.PI;
    }
    public double getOmega0(){
        return omega0;
    }

    private double setCis(){
        return toDecimalfromBinaryString(this.subFrame3.getWord(4).substring(0,16), true)
                * Math.pow(2,-29);
    }
    public double getCis(){
        return cis;
    }

    private double setI0(){
        return toDecimalfromBinaryString(this.subFrame3.getWord(4).substring(16,24)
                + this.subFrame3.getWord(5).substring(0,24), true) * Math.pow(2,-31) * Math.PI;
    }
    public double getI0(){
        return i0;
    }

    private double setCrc(){
        return toDecimalfromBinaryString(this.subFrame3.getWord(6).substring(0,16), true)
                * Math.pow(2, -5);
    }
    public double getCrc(){
        return crc;
    }

    private double setOmega(){
        return toDecimalfromBinaryString(this.subFrame3.getWord(6).substring(16,24)
                +this.subFrame3.getWord(7).substring(0,24), true) * Math.pow(2, -31) * Math.PI;
    }
    public double getOmega(){
        return omega;
    }

    private double setOmegaDot(){
        return toDecimalfromBinaryString(this.subFrame3.getWord(8).substring(0,24), true)
                * Math.pow(2,-43) * Math.PI;
    }
    public double getOmegaDot(){
        return omegaDot;
    }

    private double setIdot(){
        return toDecimalfromBinaryString(this.subFrame3.getWord(9).substring(8,22), true)
                * Math.pow(2,-43) * Math.PI;
    }
    public double getIdot(){
        return idot;
    }

    /**
     * Override toString
     * @return String of ephemeris
     */
    @Override
    public String toString() {
        String str = id +
                    "," + tow +
                    "," + wn +
                    "," + toc +
                    "," + af2 +
                    "," + af1 +
                    "," + af0 +
                    "," + crs +
                    "," + deltaN +
                    "," + m0 +
                    "," + cuc +
                    "," + ec +
                    "," + cus +
                    "," + squareA +
                    "," + toe +
                    "," + aodo +
                    "," + cic +
                    "," + omega0 +
                    "," + cis +
                    "," + i0 +
                    "," + crc +
                    "," + omega +
                    "," + omegaDot +
                    "," + idot;
        return str;
    }

    /**
     * converting to decimal from binary string either it is a binary signed number or not
     * @param intBitsStr a binary string
     * @param signed a binary signed number
     * @return
     */
    private double toDecimalfromBinaryString(String intBitsStr, boolean signed){
        long unsigned = Long.parseLong(intBitsStr,2);
        if (!signed) {
            return unsigned;
        }
        else{
            long mask = (long) Math.pow(2, (intBitsStr.length()-1));
            return -(unsigned & mask) + (unsigned & ~mask);
        }
    }

    /**
     * Convert current object to EphemerisGNSS object.
     * @return EphemerisGNSS
     */

    public GNSSEphemeris toEphemerisGNSS()
    {
        GNSSEphemeris ephemerisGNSS = new GNSSEphemeris();

        ephemerisGNSS.setPrn(this.getId());
        ephemerisGNSS.setGnssSystem(GnssStatus.CONSTELLATION_GPS);
        ephemerisGNSS.setWeek(this.getWn());
        ephemerisGNSS.setIdot(this.getIdot());
        ephemerisGNSS.setToc(this.getToc());
        ephemerisGNSS.setToe(this.getToe());
        ephemerisGNSS.setAf2(this.getAf2());
        ephemerisGNSS.setAf1(this.getAf1());
        ephemerisGNSS.setAf0(this.getAf0());
        ephemerisGNSS.setCrs(this.getCrs());
        ephemerisGNSS.setCrc(this.getCrc());
        ephemerisGNSS.setDeltaN(this.getDeltaN());
        ephemerisGNSS.setM0(this.getM0());
        ephemerisGNSS.setCuc(this.getCuc());
        ephemerisGNSS.setEc(this.getEc());
        ephemerisGNSS.setCus(this.getCus());
        ephemerisGNSS.setSquareA(this.getSquareA());
        ephemerisGNSS.setToe(this.getToe());
        ephemerisGNSS.setCic(this.getCic());
        ephemerisGNSS.setOmega0(this.getOmega0());
        ephemerisGNSS.setCis(this.getCis());
        ephemerisGNSS.setI0(this.getI0());
        ephemerisGNSS.setOmega(this.getOmega());
        ephemerisGNSS.setOmegaDot(this.getOmegaDot());

        return ephemerisGNSS;
    }

}