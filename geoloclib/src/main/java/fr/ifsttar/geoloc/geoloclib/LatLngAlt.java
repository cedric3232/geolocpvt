///=================================================================================================
// Class LatLngAlt
//      Author :  Jose Gilberto RESENDIZ FONSECA
// Modified by :  Antoine GRENIER - 2019/09/06
//        Date :  2019/09/06
///=================================================================================================
/*
 * Copyright 2018(c) IFSTTAR - TeamGEOLOC
 *
 * This file is part of the GeolocPVT application.
 *
 * GeolocPVT is distributed as a free software in order to build a community of users, contributors,
 * developers who will contribute to the project and ensure the necessary means for its evolution.
 *
 * GeolocPVT is a free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version. Any modification of source code in this
 * LGPL software must also be published under the LGPL license.
 *
 * GeolocPVT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU Lesser General Public License along with GeolocPVT.
 * If not, see <https://www.gnu.org/licenses/lgpl.txt/>.
 */
///=================================================================================================
package fr.ifsttar.geoloc.geoloclib;

import org.gogpsproject.Constants;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Class LatLngAlt
 * Calculate the coordinates geographic
 */
public class LatLngAlt {

    private double latitude;
    private double longitude;
    private double altitude;

    //----------------------------------------------------------------------------------------------

    /**
     * Constructor by default
     */
    public LatLngAlt(){
        this.latitude = 0.000000;
        this.longitude = 0.000000;
        this.altitude = 0.00;
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Constructor initializing geographic coordinates with Earth-fixed coordinates
     * @param mCoordinates Coordinates
     */
    public LatLngAlt(Coordinates mCoordinates){
        setLatLngAlt(mCoordinates.getX(),mCoordinates.getY(), mCoordinates.getZ());
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Constructor with parameters
     * @param latitude Latitude [decimal degrees]
     * @param longitude Longitude [decimal degrees]
     * @param altitude Altitude [m]
     */
    public LatLngAlt(double latitude, double longitude, double altitude){
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Method to change coordinates cartesian to coordinates geographic
     * @param x ECEF x
     * @param y ECEF y
     * @param z ECEF z
     */
    private void setLatLngAlt(double x, double y, double z) {
        //WGS84 ellipsoid constants
        int a = 6378137;
        double b = 6356752.314245;
        double e_2 = 1 - (Math.pow(b,2)/Math.pow(a,2));

        double r = Math.sqrt(Math.pow(x,2) + Math.pow(y,2));

        double lambda = Math.atan2(y,x);
        double phi = Math.atan2(z, (1 - e_2) * r);
        double h = 0;

        double diff = 1;
        double prevPhi = 0;
        while(diff > 10e-10)
        {
            prevPhi = phi;

            double N = a / Math.sqrt(1 - e_2 * Math.pow(Math.sin(phi),2));
            h = r / Math.cos(phi) - N;
            phi = Math.atan2(z, (1 - e_2 * N / (N + h)) * r);

            diff = Math.abs(prevPhi - phi);
        }

        longitude = lambda * Constants.RAD2DEG;
        latitude = phi * Constants.RAD2DEG;
        altitude = h;
    }

    //----------------------------------------------------------------------------------------------

    public double getAltitude() {
        return altitude;
    }

    //----------------------------------------------------------------------------------------------

    public double getLatitude() {
        return latitude;
    }

    //----------------------------------------------------------------------------------------------

    public double getLongitude() {
        return longitude;
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Override toString
     * @return string of coordinates geographic
     */
    @Override
    public String toString() {
        Locale currentLocale = Locale.getDefault();
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
        otherSymbols.setDecimalSeparator('.');
        NumberFormat formatAlt = new DecimalFormat("#.####", otherSymbols);
        NumberFormat formatLatLon = new DecimalFormat("#.###########", otherSymbols);
        return formatLatLon.format(latitude) + "," + formatLatLon.format(longitude) +","+ formatAlt.format(altitude);
    }

    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    public static void main(String[] args) throws IOException
    {
        Coordinates coordinates = new Coordinates(4343426.492, -124910.609, 4653463.091);

        LatLngAlt latLngAlt = new LatLngAlt();
        System.out.println(latLngAlt.toString());

        latLngAlt = new LatLngAlt(coordinates);
        System.out.println(latLngAlt.toString());

    }
}
